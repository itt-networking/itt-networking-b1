/* The configuration is adapted to a home-LAN router at 192.168.1.1/24 */
/* Interface ge-0/0/3 is set to 192.168.1.80/24 */
/* Small Office Home Office - SOHO configuration */
/* Source nat for Vlan_2 */
/* Destination nat for WebServer on 192.168.4.10 ge-0/0/2 Vlan_1 */
/* Two L2-Switches */
/* By Simon Bjerre */
version 12.1X47-D15.4;
system {
    host-name SRX_2;
    /* User: root Password: Rootpass */
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/";
    }
    services {
        ssh;
        /* dhcp service for Vlan_2 Legacy configuration */
        dhcp {
            router {
                192.168.3.1; /* Default Gateway */
            }
            pool Vlan_2 { /* Serve DHCP to this network. */
                address-range low 192.168.3.10 high 192.168.3.20;
                maximum-lease-time 3600;
                default-lease-time 3600;
                name-server {
                    8.8.8.8; /* DNS server */
                }
            }
        }
    }
}
interfaces {
    /* Vlan_2 */
    ge-0/0/1 {
        unit 0 {
            family inet {
                address 192.168.3.1/24;
            }
        }
    }
    /* Interface for Vlan_1 */
    ge-0/0/2 {
        unit 0 {
            family inet {
                address 192.168.4.1/24;
            }
        }
    }
    /* school network */
    ge-0/0/3 {
        unit 0 {
            family inet {
                address 10.56.16.80/22;
            }
        }
    }
    /* Switch 1 */  
    ge-0/0/5 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members mySwitch1Vlan;
                }
            }
        }
    }
    ge-0/0/6 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members mySwitch1Vlan;
                }
            }
        }
    }
    ge-0/0/7 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members mySwitch1Vlan;
                }
            }
        }
    }
    /* Switch 2 */
    ge-0/0/10 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members mySwitch2Vlan;
                }
            }
        }
    }
    ge-0/0/11 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members mySwitch2Vlan;
                }
            }
        }
    }
    ge-0/0/12 {
        unit 0 {
            family ethernet-switching {
                vlan {
                    members mySwitch2Vlan;
                }
            }
        }
    }
}
vlans {
    mySwitch1Vlan {
        vlan-id 3; /* id number is randomly chosen */
    }
    mySwitch2Vlan {
        vlan-id 4; /* id number is randomly chosen */
    }
}
routing-options {
    /* Default route to school gateway */
    static {
        route 0.0.0.0/0 next-hop 10.56.16.1;
    }
}
security {
    nat {
        source {
            rule-set Vlan_1-to-untrusted {
                from zone Vlan_1;
                to zone untrusted;
                rule Vlan_1-rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
        source {
            rule-set Vlan_2-to-untrusted {
                from zone Vlan_2;
                to zone untrusted;
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
        destination {
            pool myWebServer {
                address 192.168.4.10/32;
            }
            rule-set myDestinationRuleSet {
                from zone untrusted;
                rule access_untrusted_to_server {
                    match {
                        destination-address 192.168.1.80/32;
                        destination-port {
                            80;/*port used for HTTP*/
                        }
                    }
                    then {
                        destination-nat {
                            pool {
                                myWebServer;
                            }
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone Vlan_2 to-zone Vlan_2 {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone Vlan_2 to-zone untrusted {
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone Vlan_2 to-zone Vlan_1 {
            policy internet-access {
                match {
                    source-address any;
                    /* WebServer from address book */
                    destination-address NGINX_server;
                    application junos-http;
                }
                then {
                    permit;
                }
            }
        }
        from-zone Vlan_1 to-zone untrusted {
            policy Vlan_1-internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone Vlan_1 to-zone Vlan_2 {
            policy Vlan_1-internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone untrusted to-zone Vlan_2{
            policy deny-any{
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone untrusted to-zone Vlan_1{
            policy WebServerPolicy {
                match {
                    source-address any;
                    /* WebServer from address book */
                    destination-address NGINX_server;
                    application junos-http;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone Vlan_2 {
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            dhcp;
                            ssh;
                        }
                    }
                }
            }
        }
        security-zone Vlan_1 {
            address-book {
                address NGINX_server 192.168.4.10/32;
            }
            interfaces {
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
        security-zone untrusted {
            interfaces {
                ge-0/0/3.0;
            }
        }
    }
}
